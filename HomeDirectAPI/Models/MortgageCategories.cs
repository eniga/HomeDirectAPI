﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeDirectAPI.Models
{
    public class MortgageCategories
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}
